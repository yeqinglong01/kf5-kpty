%global framework kpty

Name:           kf5-%{framework}
Version: 	5.55.0
Release: 	1
Summary:        KDE Frameworks 5 Tier 2 module providing Pty abstraction

License:        LGPLv2+ and GPLv2+
URL:            https://cgit.kde.org/%{framework}.git

%global majmin %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin} kf5-rpm-macros kf5-kcoreaddons-devel >= %{majmin} kf5-ki18n-devel >= %{majmin} libutempter-devel qt5-qtbase-devel

# runtime calls %%_libexexdir/utempter/utempter
Requires:       libutempter

%description
KDE Frameworks 5 tier 2 module providing Pty abstraction.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release} kf5-kcoreaddons-devel >= %{version}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
mkdir %{_target_platform}
pushd %{_target_platform}
# find_program for utempter is failing for some reason, so
# set path explicitly to known-good value
%{cmake_kf5} .. \
  -DUTEMPTER_EXECUTABLE:PATH=/usr/libexec/utempter/utempter
popd

%make_build -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang %{name} --all-name --with-man


%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license COPYING COPYING.LIB
%{_kf5_libdir}/libKF5Pty.so.5*

%files devel
%{_kf5_includedir}/kpty_version.h
%{_kf5_includedir}/KPty/
%{_kf5_libdir}/libKF5Pty.so
%{_kf5_libdir}/cmake/KF5Pty/
%{_kf5_archdatadir}/mkspecs/modules/qt_KPty.pri


%changelog
* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler
